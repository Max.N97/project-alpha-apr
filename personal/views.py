from django.shortcuts import render
from personal.models import About
from django.contrib.auth.decorators import login_required

@login_required
def show_description(request):
    descriptions = About.objects.filter(owner=request.user)
    context = {"descriptions": descriptions}
    return render(request, "personal/description.html", context)
