from django.contrib import admin
from personal.models import About

@admin.register(About)
class ProjectAdmin(admin.ModelAdmin):
    list_display = (
        "description",
    )
