from django.urls import path
from personal.views import show_description

urlpatterns = [
    path("", show_description, name="show_description")
]
