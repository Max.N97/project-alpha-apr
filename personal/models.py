from django.db import models
from django.conf import settings


class About(models.Model):
    description = models.TextField()
    owner = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        related_name="details",
        on_delete=models.CASCADE,
        null=True,
    )
